import numpy as np 
import matplotlib.pyplot as plt 
import struct
import Progressor_bar as progressbar
import pywt
from scipy.signal import argrelextrema
from scipy.signal import morlet
from Analisys_functions import * 



class Signal:
    """Classe definissant une image"""
    
    #freq , nb_points , intensité 
    def __init__ (self, signal, nom, a=0, b=1,freq = None ,nb_points = None , intensity = None , variable = None):

        if signal is None and  (freq is None or nb_points is None) : 
            print("You have to enter a signal or give at least a frequency and a number of points to create a Signal object")
            return 

        else : 
            if signal is None : 
                
                if variable is None :self.signal = signal_test(freq, a, b, nb_points,intensity)
                
                else: self.signal = signal_test_variable(freq,a,b,nb_points,ordre= variable)
                    
                self.nbPoints= nb_points
                
            else :  
                self.signal = signal
                self.nbPoints = len(signal)

            self.nom = nom
            self.a = a
            self.b = b 
            self.fe = 1/((b-a)/self.nbPoints)
            self.df = 1./(self.b-self.a)
            self.echelleTemporelle = np.linspace(self.a, self.b, self.nbPoints)
            return
    
    def rajoutBruit(self,percent):
        """rajoute un bruit gaussien au signal"""
        self.signal = self.signal + percent * np.var(self.signal) * np.random.randn(len(self.signal)) #rajout du bruit
        return
 
    

    def fft(self):
        """calcule la fft du signal"""
        return np.fft.fft(self.signal, norm="ortho")
    
    
    def fourierInverse(self):
        """calcule l'ifft, affiche l'erreur avec le signal (de base)"""
        ############################################################
        fft=self.fft() #du coup on le calcule
        ifft = np.fft.ifft(fft)
        fourierErreurRelative = np.linalg.norm(self.signal-self.ifft)/np.linalg.norm(self.signal)
        return fourierErreurRelative, ifft
   
    

    def autocor(self):
        s = self.signal
        print("Computation of the autocorralation")
    
        N=s.shape[0]
        C = np.zeros(N)
    
        counter = 0
        ProgressBar = None
        for t in range(N):
        
            #progression bar
            #---------------------------------------------------------------------------------------------------------
            counter += 1
            label = str(round(t/N *100,1)) + "%"
            if ProgressBar == None:
            #Si on a pas encore construit la barre
                ProgressBar = progressbar.ProgressBar(counter, N,fillingChar = 'x', label = label, usePercentage = False)
            else:
            #Si on a déjà construit la barre
                ProgressBar.updateProgress(counter, label)
            #---------------------------------------------------------------------------------------------------------
        
            s_trans = np.zeros(N)
            s_trans[:N-t:] = s[t::]
            C[t] += sum(s*s_trans)/(N-t)
        
        return C

        
    def dsp(self,autocorr = None ):
        """calcule la dsp"""
        if autocorr is None : return np.fft.fft(self.autocor())
        else: return np.fft.fft(autocorr)
 
    def variation(self): 
        s = self.signal
        grad = (np.roll(s,-1) - s)[:-1]
        grad[grad>0]=1
        grad[grad<0]=-1

        return Signal(grad, self.nom + " variations", a=self.a, b=self.b,freq = None ,nb_points = None , intensity = None)


    def analyse_signal(self,nb_peaks,window_type,time_unit = "s", plot_an = "all" , show = True): 
        s = self.signal
        a = self.a  
        b = self.b
        signal_name =self.nom
        #temporal discretisation  
        dt = (b-a)/len(s)
        t = np.linspace(a,b,len(s))
    
        #frequential discretisation 
        fe = 1/dt
        df = 1/(b-a)
        f = np.fft.fftfreq(len(t))
        # we multiply by fe because the vector returned bu fftfreq is unitary
        # so we have to adapt it to our sampling frequency
        f_print = f[f>=0]*fe 
        

        autocorr=None 


        if plot_an== "all" :
            plt.figure(figsize=(17,5))
            plt.subplot(2,2,1)
            plt.subplots_adjust(top=0.95, hspace = 1)
            plot_signal(t, s, "time ("+time_unit+")", "Magnitude", signal_name, show =False)
            

        if plot_an== "all" or plot_an== "fft": 
            #fourrier transfom and spectrum of the signal
            fft = np.fft.fft(s)
            spr_s = np.sqrt(fft.real**2 + fft.imag**2)[f>=0]
            spr_s[0]=0
            peaks_spr = select_peaks(spr_s,nb_peaks,window_type)

            if plot_an== "all" : plt.subplot(2,2,2)
            plot_signal(f_print, 20*np.log10(spr_s), "Frequency (1/"+time_unit+")", "Magnitude (dB)", "Spectrum of "+signal_name, show =False)
            plt.scatter(f_print[peaks_spr[:,1].astype(int)], 20*np.log10(peaks_spr[:,0]),c='r')
            
            print("Peaks : \nFourrier Transform : ",f_print[peaks_spr[:,1].astype(int)].astype(str)) 


        if plot_an== "all" or plot_an== "corr": 
            #autocorrelation and PSD of the signal
            
            autocorr = self.autocor()
            
            if plot_an== "all" : 
                plt.subplot(2,2,3)
                plt.subplots_adjust(hspace = 0.5)
            
#            plot_signal(np.arange(len(s)), autocorr, "Shift (number index)", "Autocorralation value", "Autocorrelation of "+signal_name, show =False)
            plot_signal(self.echelleTemporelle, autocorr, "Shift time ("+time_unit+")", "Autocorralation value", "Autocorrelation of "+signal_name, show =False)
            #plt.plot(self.echelleTemporelle, autocorr)
            #print("test fichier")

        if plot_an== "all" or plot_an== "psd": 
            #autocorrelation and PSD of the signal 
            temp = self.dsp(autocorr)
            PSD = np.sqrt(temp.real**2 + temp.imag**2)[f>=0]
            peaks_PSD = select_peaks(PSD,nb_peaks,window_type)

            if plot_an== "all" : 
                plt.subplot(2,2,4)
            plot_signal(f_print, 20*np.log10(PSD),"Frequency (1/"+time_unit+")", "Magnitude (dB)", "PSD of "+signal_name, show =False)
#            print("peaks_PSD[:,1]", peaks_PSD[:,1])
#            print("20*np.log10(peaks_PSD[:,0])", 20*np.log10(peaks_PSD[:,0]))
            if(len(peaks_PSD) > 0):
                plt.scatter(f_print[peaks_PSD[:,1].astype(int)], 20*np.log10(peaks_PSD[:,0]),c='r')
                print("Power Spectral Density : ",f_print[peaks_PSD[:,1].astype(int)].astype(str))
    
    

        if show : plt.show()
        print("\n")

        if plot_an== "all" and len(peaks_spr) > 0 and len(peaks_PSD) > 0:
            return f_print[peaks_spr[:,1].astype(int)],  f_print[peaks_PSD[:,1].astype(int)] 
        
    # zoom_petites_freq permet de prendre en compte les 1/zoom_petites_freq premières fréquences 

    def analyse_temp_freq(self,nb_fenetre, zoom_petites_freq = None,time_unit = "day" ): 
        N=int(len(self.signal))
        lf = N//nb_fenetre
        Fe=self.fe
        n=lf/8 #recouvrement de 7/8 donc decalage de 1/8
        T=2*lf/Fe #Largeur temporelle de la fenêtre
        F=np.arange(lf)/T #Vecteur des fréquences observées
        t=[] #vecteur des temps
        S=np.zeros(lf) #matrice des coefficients de Fourier
        w=np.hanning(2*lf)
        
        if(lf+1 >= N-lf+1):
            print("La taille de la fenête est trop grande.")
        for k in np.arange(lf,N-lf,lf-n):
            
            t=np.hstack((t,[(k*N/(N-2*lf)-N*(lf)/(N-2*lf))/Fe])) #construction du vecteur temps
            xw=w*self.signal[int(k-lf):int(k+lf)] #produit de la fenetre avec le signal
            yw=np.fft.fft(xw)/len(xw) # transformée de Fourier
            S=np.vstack((S,np.abs(yw[:lf])))
        X,Y=np.meshgrid(t,F)      
        Z=S[1:,]
        plt.figure(figsize=(15,6))
        
        if zoom_petites_freq is None : CS=plt.contourf(X,Y,Z.T,10,cmap=plt.cm.bone)

        else : 
            l = Z.shape[1]//zoom_petites_freq
            plt.figure(figsize=(15,6))
            CS=plt.contourf(X[:l,:],Y[:l,:],Z.T[:l,:],10,cmap=plt.cm.bone)
            
            
            
        cbar=plt.colorbar(CS)
        cbar.ax.tick_params(labelsize=13)
        plt.xlabel('Time ('+time_unit+')',fontsize=13)
        plt.ylabel('frequency (1/'+time_unit+')',fontsize=13)
        plt.title('Time-Frequency diagram',fontsize=14,fontweight="bold")
        plt.xticks(fontsize=13)
        plt.yticks(fontsize=13)
        plt.show()
#===============================================================================================
    #Permet de faire une analyse temp fréquence.
    # zoom freq représente la fréquence maximale que l'on veut afficher
    # Nb freq représente le nombre de fréquences les plus fortes que l'on veut afficher
    def analyse_temp_freq_ond(self, ond='morl', zoom_freq= None,nb_freq_and_res = None,time_unit='s'): 
        a = self.a
        b = self.b 
        s = self.signal

        #définition du vecteur temporel
        dt = (b-a)/len(s)
        T = np.arange(a,b,dt)

        fe = 1/dt #frequence d'echantillonnage

        #calcul des echelle pour avoir une fréquence minimal égale à la taille du signal
        #-------------------------------------------------------------------------------
        #premier approx de la scale max, optimiste
        n_max_init = int(fe*(b-a))#(np.log(fe)+np.log(b-a))/np.log(2)
        f_init = pywt.scale2frequency(ond, np.arange(1,n_max_init))*fe

        #estimation exacte de n
        n_max = len(f_init[f_init>=0.5])
        scales = np.arange(1,n_max)   

        #echelle fréquentielle
        F = pywt.scale2frequency(ond, scales) / dt
        #--------------------------------------------------------------------------------


        #Transformée en ondelette du sigtnal
        W = pywt.cwt(s, scales, ond)

        #Module de la tranformée
        N = (np.real(W[0])**2+np.imag(W[0])**2)**0.5


        #récupère la ligne de N qui a la norpme la plus grande 
        #----------------------------------------------------
        if nb_freq_and_res is not None :
            nb_freq = nb_freq_and_res[0]
            res = nb_freq_and_res[1]
            List_int_freq = []
            for i in range(N.shape[0]) : 
                List_int_freq.append([np.linalg.norm(N[i]),i])


            List_int_freq.sort(reverse = True )
            freq = F[np.asarray(List_int_freq)[:,1].astype('int')]
            
            # récupère les fréquences les plus forte en intensité selon une certaine résolution
            print('Most important frequencies : ')
            big_freq =np.ones(nb_freq)* freq[0]  # va contenir les fréquences les plus fortes, on prend d'office la première
            nb_freq_ = 1#nombre de frequences enregistrées
            i = 1 #indice de la frequence que l'on traite 
            
            while nb_freq_ < nb_freq and i<len(freq)-1 : 
                
                #si on a déjà une fréquence proche en terme de résolution fréquentielle on ne fait rien 
                if len(big_freq[abs(big_freq-freq[i])<res])> 0 : 
                    None
                #Sinon on enregistre   
                else : 
                    big_freq[nb_freq_] = freq[i]#print(freq[i])
                    nb_freq_ +=1 
                
                i += 1 
 
                    
            print(big_freq[:nb_freq_])
        #----------------------------------------------------

        #affichage 
        #----------------------------------------------------
        plt.figure(figsize=(15,6))

        if zoom_freq is not None : 
            ind_max = len(F[F<=zoom_freq])#np.shape(N)[0]//zoom
            plt.pcolormesh(T, F[-ind_max:], N[-ind_max:,:])
            plt.xlabel("Time ("+time_unit+")",fontsize=15)
            plt.ylabel("Frequency (1/"+time_unit+")",fontsize=15)

        else : plt.pcolormesh(T, F, N)

        plt.title("Time Frequency analysis for " + self.nom,fontsize=17,fontweight="bold")
        cbar= plt.colorbar()
        cbar.ax.tick_params(labelsize=15)
        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
        plt.show()

    
    #===============================================================================================
    
    #===============================================================================================
    # Pour afficher un objet signal
    def afficher_signal(self,time_unit='s'):
        s = self.signal
        x= np.linspace(self.a,self.b,len(s))
        plot_signal(x, s,"Time ("+time_unit+")"," ", title = self.nom, show =True)


def signaux_test(list_freq, list_a,list_b,list_nb_points,intensity_list,name_list):
    """generates a list of signal_test by taking lists of frequencies time interval, number of points and intensity"""
    signaux =[]

    for i in range(len(list_freq)) : 
        signaux.append(Signal(signal = None , a=list_a[i], b=list_b[i], freq = list_freq[i], nb_points = list_nb_points[i],intensity =intensity_list[i],nom =name_list[i] ) )

    return signaux
