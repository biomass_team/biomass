import numpy as np
import matplotlib.pyplot as plt 
import struct
import Progressor_bar as progressbar
import pywt
from scipy.signal import argrelextrema
from scipy.signal import morlet



#def signal_test(F, n=50):
#    """permet de generer un signal compose d une somme de sinusoide de frequences les elements de F"""
#    signal = np.zeros(n)
#    X = np.arange(n)
#    for i in range(len(F)):
#        signal += np.sin(F[i]*X)
#    return signal   



def ouverture_fichier(nom):
    """importe un fichier"""
    f = open(nom, "r") 
    data_list = []
    for line in f : 
        data_list.append(line.split())
    return data_list



def representation(x, signal, nom, t, ylabel):
    n = len(signal)
    #transforme la liste de donnee en matrice
   ###########" T = np.arange(0, t*n, t)# temps
    T = x
    #mat_data = np.asarray(data_list)
    #on recupere les donnees numeriques 
    #data_val = mat_data[:,2].astype(float)

    #plt.figure(figsize=(15,8))
    plt.plot(T, signal)
    plt.title('Signal "'+nom+'"')
    plt.ylabel(ylabel)
    plt.xlabel("Temps en minutes")
    #plt.show()



#fonction qui calcule l autocorrelation
def autocorr(x):
    result = np.correlate(x, x, mode='full') #############################################
    return result[result.size//2:]

def autocorrel(x):#,N,i,M):
    N = len(x)
    i = 0
    M = len(x)
    C = np.zeros(N)
    for k in range(i,i+M):
        for n in range(N):
            C[n] += x[k]*x[k-n]
    return C/M


def autocorrel_optimise(x):#,N,i,M):
    N = len(x)
    C = np.zeros(N)
    S = np.zeros(N) #vecteur "decale"
    C[0] = sum(x*x) # sans decalage
    for n in range(1,N):
        S[:n:] = x[-n::] 
        S[n::] = x[:N-n:]
        #print(sum(x)-sum(S))
        C[n] = sum(x*S)
    return C/(C[0]) # Normalisation

#autocorrel_optimise(signal)





def calc_hanning(m,n):
    return .5*(1 - np.cos(2*np.pi*np.linspace(1,m,m)/(n+1)))

def hanning(n):
    if (n%2) == 0:#multiple de 2
        # Even length window
        half = n//2
        w = calc_hanning(half,n)
        w = np.concatenate((w,w[::-1]))
    else:
        # Odd length window
        half = (n+1)//2
        w = calc_hanning(half,n)
        z=w[::-1]
        w = np.concatenate((w,z[1:]))
    return w


def f_hanning(signal,taille_support=96):
    if(taille_support//8!=taille_support/8):
        print("Choisir une taille de support divisible par 8 !")
    ns = len(signal)
    n = (8*ns)//taille_support
    H = hanning(taille_support)
    Spec=np.zeros((taille_support,n))
    Phase=np.zeros((taille_support,n))
    for i,j in enumerate(range(0,ns-taille_support, taille_support//8)):
        temp = H*signal[j:j+taille_support]
        ftemp=np.fft.fft(temp)
        Spec[:,i]=np.abs(ftemp)
        Phase[:,i]=np.angle(ftemp)
    return Spec, Phase


def decomposition_en_fenetre(S, nbre_fenetre):
    Fenetre = []
    for i in range(nbre_fenetre):
        Fenetre += [S[len(S)//nbre_fenetre*(i):len(S)//nbre_fenetre*(i+1)]] 
    return Fenetre   





def Hanning_v2(N): 
    x= np.arange(N)
    return 0.54 + 0.46*np.cos(2*np.pi*(x+N/2)/N)

def Blackman(N): 
    x = np.arange(N)
    return 0.42 + 2* (0.25*np.cos(2*np.pi*(x+N/2)/N)+0.04*np.cos(4*np.pi*(x+N/2)/N))



def select_peaks(s,nb_peaks,window_type,N): 
    
    # DEFINITION OF THE AMPLITUDE AND FREQUENCY RESOLUTION 
    #---------------------------------------------------------------    
    if window_type == "hanning_2" : 
        ampl_res = 43
        freq_res = 4/N
        
    elif window_type== "blackman" : 
        ampl_res = 59
        freq_res = 6/N
        
    elif window_type== "rectangular" : 
        ampl_res = 13
        freq_res = 2/N
        
    else : 
        print("The window type used is not known")
        return None 
    #---------------------------------------------------------------
    
    #list that will be filled with the peaks
    peaks_list = []
    
    
    s_db= 20*np.log10(s) 
    max_peak = max(s_db)
    
    #if there is a peak at 0 we save it
    #rajouter cond sur resolution amplitde
    if s_db[0]>s_db[1]: peaks_list.append([s_db[0],0])
    for i in range(1,s.shape[0]-1): 
        #determine if there is a peak at the current index 
        if s_db[i]>s_db[i-1] and s_db[i]>s_db[i+1] and max_peak-s_db[i]<ampl_res: 
            #if there is already some peaks in the list we add this one 
            if len(peaks_list)==0 : peaks_list.append([s_db[i],i])
            
            else : 
                
                #if the peak is higher than the previous one but represents the same frequency 
                #according to the resolution in frequency or amplitude then we replace the last one
                # by this new one
                
                if  s_db[i]>peaks_list[-1][0] and np.abs(i-peaks_list[-1][1])<freq_res : #s_db[i]-peaks_list[-1][0] > ampl_res or
                    peaks_list[-1] = [s_db[i],i]
                
                #else if it represents a new frequency we add it to the list 
                
                elif np.abs(i-peaks_list[-1][1])>=freq_res : #np.abs(s_db[i]-peaks_list[-1][0]) <= ampl_res
                    peaks_list.append([s_db[i],i])
                    
    #for the last element        
    if s_db[-1]>s_db[-2]: 
        if s_db[-1]-peaks_list[-1][0] > ampl_res or np.abs(s_db.shape[0]-1-peaks_list[-1][1])<freq_res :
            peaks_list[-1] = [s_db[-1],s_db.shape[0]-1]
        else : peaks_list.append([s_db[-1],s_db.shape[0]-1])
        
    peaks_list.sort(reverse=True)
    
    if len(peaks_list) > nb_peaks : 
        res = peaks_list[:nb_peaks]
        
    else: res = peaks_list
    
    res = np.asarray(res)
    if len(res.shape) > 1 : res[:, 0] = 10**(res[:, 0]/20)
    else : res[0] = 10**(res[0]/20)
    return res



def autocorr(s):
    N=s.shape[0]
    C = np.zeros(N)
    for t in range(N):
        s_trans = np.zeros(N)
        s_trans[:N-t:] = s[t::]
        C[t] += sum(s*s_trans)/(N-t)
    return C



