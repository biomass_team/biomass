import numpy as np 
import matplotlib.pyplot as plt
import Progressor_bar as progressbar


import struct
from scipy.signal import argrelextrema
from scipy.signal import morlet
import pywt



#===========================================================================================================
def signal_test_variable(freq,a,b,nb_pnts,ordre= 1): 

    t =  np.linspace(a,b,nb_pnts)

    f=freq*t**ordre
    sigusoid_freq_var = np.sin(2*np.pi*t*(f))
    
    return sigusoid_freq_var

#===========================================================================================================

#===========================================================================================================

def signal_test(F, a, b, nb_points,intensity=None):
    """permet de générer un signal compposé d'une somme de sinusoide de fréquences les éléments de F"""
    signal = np.zeros(nb_points)
    X = np.linspace(a,b,nb_points)
    if intensity is None : 
        for i in range(len(F)):
            signal += np.sin(2*np.pi*F[i]*X)
            
    else : 
        for i in range(len(F)):
            signal += intensity[i]*np.sin(2*np.pi*F[i]*X)
            
    return signal 

#===========================================================================================================


#===========================================================================================================

def select_peaks(s,nb_peaks,window_type): 
    
    # DEFINITION OF THE AMPLITUDE AND FREQUENCY RESOLUTION 
    #---------------------------------------------------------------    
    if window_type == "hanning_2" : 
        ampl_res = 43
        freq_res = 4
        
    elif window_type== "blackman" : 
        ampl_res = 59
        freq_res = 6
        
    elif window_type== "rectangular" : 
        ampl_res = 13
        freq_res = 2
        
    else : 
        print("The window type used is not known")
        return None 
    #---------------------------------------------------------------
    
    #list that will be filled with the peaks
    peaks_list = []
    
    
    s_db= 20*np.log10(s) 
    max_peak = max(s_db)
    # if the maximum peak is at the firt element throw a warning 
    # if the difference wit the second one is higher than the magnitude resolution we neglect it 
    # so that the function does not return an empty list 
    if max_peak ==  s_db[0] : 
        peaks_list.append([s_db[0],0])
        if max_peak - max(s_db[1:]) >= ampl_res :
            max_peak = max(s_db[1:])
            print("WARNING : Maximum peak at 0, and difference with the second one is higher than the magnitude resolution")
        else : print("WARNING : Maximum peak at 0")
    
    for i in range(1,s.shape[0]-1): 
        #determine if there is a peak at the current index 
        if s_db[i]>s_db[i-1] and s_db[i]>s_db[i+1] and max_peak-s_db[i]<ampl_res: 
            #if there is already some peaks in the list we add this one 
            if len(peaks_list)==0 : peaks_list.append([s_db[i],i])
            
            else : 
                
                #if the peak is higher than the previous one but represents the same frequency 
                #according to the resolution in frequency or amplitude then we replace the last one
                # by this new one
                
                if  s_db[i]>peaks_list[-1][0] and np.abs(i-peaks_list[-1][1])<freq_res : #s_db[i]-peaks_list[-1][0] > ampl_res or
                    peaks_list[-1] = [s_db[i],i]
                
                #else if it represents a new frequency we add it to the list 
                
                elif np.abs(i-peaks_list[-1][1])>=freq_res : #np.abs(s_db[i]-peaks_list[-1][0]) <= ampl_res
                    peaks_list.append([s_db[i],i])
                    
        
    peaks_list.sort(reverse=True)
    
    if len(peaks_list) > nb_peaks : 
        res = peaks_list[:nb_peaks]
        
    else: res = peaks_list
    
    res = np.asarray(res)
    if len(res.shape) > 1  : res[:, 0] = 10**(res[:, 0]/20)
    elif len(res.shape)  == 1 and len(res)>=1 : 
        print("ail")
        res[0] = 10**(res[0]/20)
    
    return res

#===========================================================================================================


#===========================================================================================================
def moyenne_glissante(signal, taille_fenetre):
    signal_moyenne = np.zeros(len(signal))
    for i in range(len(signal)):
        signal_moyenne[i] = np.roll(signal, -i+taille_fenetre)[:taille_fenetre*2:].mean()
    return signal_moyenne
#===========================================================================================================


#===========================================================================================================

def suppr_peaks(signal,taille_fenetre , seuil): 

    signal_nettoyer = signal.copy()
    signal_moyenne = moyenne_glissante(signal, taille_fenetre)
    nbre_Pics = 0
    for i in range(len(signal)):
        if(np.linalg.norm(signal[i] - signal_moyenne[i]) > seuil):
            nbre_Pics += 1
            signal_nettoyer[i] = signal_moyenne[i]   # faut faire la moyenne sans les pics, de plus y a potentiellemnt deux pivs qui se suivent, il faut donc enlever tou les pics puis leur mettre de novelles valeurs
    print("Le nombre de pics supprimé est ", nbre_Pics, " soir un pourcentage ", nbre_Pics/len(signal_nettoyer))
    
    return signal_nettoyer

#===========================================================================================================


#===========================================================================================================

def plot_signal(x, y, x_lab, y_lab, title, show =True) : 
    if show : 
        plt.figure(figsize=(17,8))
        plt.subplots_adjust(hspace = 0.5)
    plt.plot(x,y)        
    plt.xlabel(x_lab,fontsize=13)
    plt.ylabel(y_lab,fontsize=13)
    plt.title(title,fontsize=14,fontweight="bold")
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    if show : plt.show()
        
#===========================================================================================================

#===========================================================================================================

def ouverture_fichier(nom):
    """importe un fichier"""
    f = open(nom, "r") 
    data_list = []
    for line in f : 
        data_list.append(line.split())
    return data_list