import numpy as np
import matplotlib.pyplot as plt
import struct
import scipy

#from Signal_v2 import * 

#import pywt
exec(open("Signal_v2.py").read(), globals()) # réexecuter le cellule permet de prendre en compte les modifs du fichiers fonctions.py
exec(open("Analisys_functions.py").read(), globals()) 


FREQ = [[1],[1, 50, 10],[1,5,10,20,50,100,200,500],[1, 3, 5, 7, 9, 11, 13, 15, 17, 19]]
NOM = ["Sinusoide","Sinusoide triple","Sinusoide octuple",'Sinusoide crenau']
INTENSITY = [None, None,None,[4, 4./3, 1, 4./5, 4./7, 4./9, 4./11, 4./13, 4./15, 4./17, 4./19]]
A = [0,0,0,0]
B = [1,5,10,10]
NB_POINTS = [100,1000,10000,10000]
signaux = signaux_test(FREQ, A,B,NB_POINTS,INTENSITY,NOM)

#signaux physique
data_list0 = ouverture_fichier("bt0VV_4lks_d45-60_dt4h.dat")
data_list1 = ouverture_fichier("tst.dat")

dt0 = 15/(60*24)  # mesure toutes les 15 minutes, échelles temporelles en jours
signaux += [Signal(signal = np.asarray(data_list0)[:,2].astype(float),nom= "signal radar", a=0, b=dt0*len(data_list0))]
#humidite = []
for i,j in enumerate(range(2,11)):
    s = np.asarray(data_list1)[:,j].astype(float)
    nom = 'fichier "tst.dat" signal '+str(i)
    dt1 = 10/(60*24)  # mesure toutes les 10 minutes, échelles temporelles en jours
    signal = Signal(s, nom, a=0, b=dt1*len(data_list1))
    #humidite += [signal]
    signaux += [signal]
